﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//enums are fixed states, like Day/Night
namespace CoAHomework
{
    public enum PlayerState 
    {
        NONE,
        IDLE,
        WALKING,
        RUNNING,
    }

}