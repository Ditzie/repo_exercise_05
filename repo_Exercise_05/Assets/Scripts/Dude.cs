﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework 
{

    public class Dude : MonoBehaviour
    {
        public PlayerState state;
        
        private float movespeed;

        void Update()
        {
            //basically, more complex if/else
            switch (state)
            {
                case PlayerState.NONE:
                    movespeed = 0;
                    //dont forget to break,prone for bugs..
                    break;                    
                case PlayerState.IDLE:
                    movespeed = 0;
                    break;
                case PlayerState.WALKING:
                    movespeed = 15;
                    break;
                case PlayerState.RUNNING:
                    movespeed = 30;
                    break;
                default:
                    break;
            }
        // + to build strings
            Debug.Log("State is "+ state+ " with " + movespeed);
        }


    }
}
