﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class friend : MonoBehaviour
    {
        public bool ishungry;
        public bool hasToPee;
        public bool isSleepy;
        
        private void DoStuff()
        {
            //&& to keep checking. & just checks one

            if(ishungry&&hasToPee&&isSleepy)
            {
                Debug.Log("Roll over and cry");
            }
            else
            {
                Debug.Log("Keep going");
            }

            if(ishungry&&hasToPee||ishungry&&isSleepy||hasToPee&&isSleepy)
            {
                Debug.Log("Have some Text");
            }

            else
            {
                Debug.Log("Yeah,I have no idea");
            }
        }

        private void DoThings()
        {
            if(ishungry||hasToPee||isSleepy)
            {
                Debug.Log("I think I'm supposed to print something");
            }

        }
    
        void Update()
        {
            DoStuff();
            DoThings();
        }
    }

}